#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>

using namespace std;

long go(long a, long b) {
    if (b == 0) return 1;
    if (b == 1) return a;
    long foo = go(a, b / 2);
    constexpr long MOD = 1e9 + 7;
    return ((foo * foo % MOD) * (b % 2 == 0 ? 1 : a)) % MOD;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        int a, b;
        cin >> a >> b;
        std::cout << go(a, b) << '\n';
    }
    return 0;
}

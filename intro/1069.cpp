#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>

using namespace std;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    string s;
    cin >> s;
    char prev = 0;
    int max = 0;
    int same = 1;
    for (char c : s) {
        if (c == prev) same++;
        else same = 1;
        prev = c;
        max = std::max(max, same);
    }
    cout << max << endl;
    return 0;
}

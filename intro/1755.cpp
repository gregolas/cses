#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>
#include <string>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    string s;
    cin >> s;
    vector<int> freq(26);
    for (char c : s) {
        freq[c - 'A']++;
    }
    string ans;
    ans.resize(s.size());
    int idx = 0;
    int odds = 0;
    for (int i = 0; i < freq.size(); i++) {
        if (freq[i] % 2 != 0) {
            ans[ans.size() / 2] = i + 'A';
            freq[i]--;
            if (++odds > (s.size() % 2 != 0)) {
                cout << "NO SOLUTION\n";
                return 0;
            }
        }
        while (freq[i]) {
            ans[idx] = i + 'A';
            ans[ans.size() - 1 - idx] = i + 'A';
            idx++;
            freq[i] -= 2;
        }
    }
    cout << ans << '\n';
    return 0;
}

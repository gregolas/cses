#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

inline unsigned long read(){
	unsigned long t = 0;
	char c;
	while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
	//while((c=getchar_unlocked()) >= 48) {
		t=t*10+c-48;
	}
	return t;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    unsigned long n = read();
    unsigned long sum = 0;
    for (int i = 0; i < n - 1; i++) {
        sum += read();
    }
    cout << (n * (n + 1) / 2 - sum) << endl;
    return 0;
}

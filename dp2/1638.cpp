#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

constexpr int mod = 1e9 + 7;

template <typename T = int>
inline T read(){
  T t = 0;
  char c;
  while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
    t=t*10+c-48;
  }
  return t;
}

int main(void) {
  //ios_base::sync_with_stdio(false);
  //cin.tie(NULL);
  int n = read();
  std::vector<std::vector<char>> v(n, std::vector<char>(n));
  std::vector<std::vector<int>> dp(n + 1, std::vector<int>(n + 1, 0));
  for (int i = 0; i < n; i++) for (int j = 0; j < n; j++) cin >> v[i][j];
  dp[1][1] = v[0][0] != '*';
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n; j++) {
      if (v[i - 1][j - 1] == '*') continue;
      dp[i][j] += dp[i - 1][j];
      dp[i][j] %= mod;
      dp[i][j] += dp[i][j - 1];
      dp[i][j] %= mod;
    }
  }
  std::cout << dp.back().back() << '\n';
  return 0;
}

#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

template <typename T = int>
inline T read(){
  T t = 0;
  char c;
  while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
    t=t*10+c-48;
  }
  return t;
}

int main(void) {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n = read();
  int x = read();
  std::vector<int> coins(n);
  std::vector<int> dp(x + 1);
  for (int i = 0; i < n; i++) {
    int c = read();
    if (c > x) continue;
    coins[i] = c;
  }
  //std::sort(coins.begin(), coins.end());
  dp[0] = 1;
  int mod = 1e9 + 7;
  for (int i = 0; i <= x; i++) {
    for (auto &c : coins) {
      if (i - c >= 0) {
        dp[i] += dp[i - c];
        dp[i] %= mod;
      }
    }
  }
  std::cout << dp.back() << '\n';
  return 0;
}

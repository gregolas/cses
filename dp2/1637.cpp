#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

constexpr int mod = 1e9 + 7;

template <typename T = int>
inline T read(){
  T t = 0;
  char c;
  while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
    t=t*10+c-48;
  }
  return t;
}

const std::vector<int> &digits(int n) {
  static std::vector<int> v;
  v.clear();
  while (n) {
    v.emplace_back(n % 10);
    n /= 10;
  }
  return v;
}

int main(void) {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n = read();
  std::vector<int> dp(n + 1, 1e9);
  dp[n] = 0;
  for (int i = n; i >= 0; i--) {
    const auto &d = digits(i);
    for (auto &digit : d) {
      if (i - digit >= 0) {
        dp[i - digit] = std::min(dp[i - digit], dp[i] + 1);
      }
    }
  }
  std::cout << dp[0] << '\n';
  return 0;
}

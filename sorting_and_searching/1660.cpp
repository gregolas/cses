#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

inline int read(){
	int t = 0;
	char c;
	while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
		t=t*10+c-48;
	}
	return t;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    unsigned n, x;
    n = read();
    x = read();
    vector<unsigned> v(n);
    for (unsigned i = 0; i < n; i++) {
        v[i] = read();
    }
    unsigned p1 = 0;
    unsigned p2 = 0;
    unsigned sum = v[0];
    unsigned ans = 0;
    while (p2 < v.size()) {
        ans += sum == x;
        if (sum >= x) sum -= v[p1++];
        else sum += v[++p2];
    }
    std::cout << ans << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    long n, x;
    cin >> n >> x;
    map<long, long> counts;
    long sum = 0;
    long ans = 0;
    counts[0] = 1;
    for (int i = 0; i < n; i++) {
        long curr;
        cin >> curr;
        sum += curr;
        long target = sum - x;
        if (auto it = counts.find(target); it != counts.end()) {
            ans += it->second;
        }
        counts[sum] += 1;
    }
    std::cout << ans << '\n';
    return 0;
}

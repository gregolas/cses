#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    long n;
    cin >> n;
    vector<long> counts(n);
    long sum = 0;
    long ans = 0;
    counts[0] = 1;
    for (int i = 0; i < n; i++) {
        long curr;
        cin >> curr;
        curr %= n;
        if (curr < 0) curr += n;
        sum += curr;
        sum %= n;
        ans += counts[sum]++;
    }
    std::cout << ans << '\n';
    return 0;
}

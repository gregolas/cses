#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, k;
    cin >> n >> k;
    ll left_sum = 0;
    ll right_sum = 0;
    multiset<ll> left;
    multiset<ll> right;
    auto add_left = [&left, &left_sum](ll n) {
        left.insert(n);
        left_sum += n;
    };
    auto add_right = [&right, &right_sum](ll n) {
        right.insert(n);
        right_sum += n;
    };
    auto remove = [&left, &right, &left_sum, &right_sum](ll val) {
        if (left.size() and val <= *left.rbegin()) {
            left_sum -= val;
            left.erase(left.find(val));
        } else {
            right_sum -= val;
            right.erase(right.find(val));
        }
    };
    auto rotate_right = [&left, &right, &left_sum, &right_sum]() {
        left_sum -= *left.rbegin();
        right_sum += *left.rbegin();
        right.insert(*left.rbegin());
        left.erase(prev(left.end()));
    };
    auto rotate_left = [&left, &right, &left_sum, &right_sum]() {
        right_sum -= *right.begin();
        left_sum += *right.begin();
        left.insert(*right.begin());
        right.erase(right.begin());
    };
    vector<ll> v(n);
    for (int i = 0; i < n; i++) {
        ll val;
        cin >> val;
        v[i] = val;
        if (left.empty() and right.empty()) {
            add_left(val);
        } else if (left.size()) {
            if (val <= *left.rbegin()) {
                add_left(val);
            } else {
                add_right(val);
            }
        } else if (right.size()) {
            if (val >= *right.begin()) {
                add_right(val);
            } else {
                add_left(val);
            }
        }
        while (int(left.size()) - int(right.size()) > 1) {
            rotate_right();
        }
        while (int(right.size()) > int(left.size())) {
            rotate_left();
        }
        if (i + 1 >= k) {
            ll cost = 0;
            if (k % 2 == 0) {
                ll median = (*left.rbegin());
                if (right.size()) {
                    median += *right.begin();
                    median /= 2;
                }
                if (left.size())
                    cost += (k / 2 - 1) * median - left_sum;
                if (right.size())
                    cost += right_sum - (k / 2 - 1) * median;
            } else {
                ll median = *left.rbegin();
                if (left.size() > 1)
                    cost += right_sum - (k / 2) * median;
                if (right.size())
                    cost += (k / 2) * median - (left_sum - *left.rbegin());
            }
            cout << cost << ' ';
            if (i - k + 1 >= 0)
                remove(v[i - k + 1]);
        }
    }
    return 0;
}

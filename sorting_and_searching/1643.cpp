#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    vector<int> v(n);
    for (int i = 0; i < n; i++) {
        cin >> v[i];
    }
    long long sum = max(0, v[0]);
    long long ans = v[0];
    for (int i = 1; i < n; i++) {
        sum += v[i];
        ans = std::max(ans, sum);
        if (sum < 0) sum = 0;
    }
    std::cout << ans << '\n';
    return 0;
}

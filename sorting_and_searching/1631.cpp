#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    long max = 0;
    long sum = 0;
    for (int i = 0; i < n; i++) {
        long a;
        cin >> a;
        max = std::max(max, a);
        sum += a;
    }
    std::cout << std::max(2 * max, sum) << '\n';
    return 0;
}

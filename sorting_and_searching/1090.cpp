#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, x;
    cin >> n >> x;
    vector<int> p(n);
    for (int i = 0; i < n; i++) cin >> p[i];
    std::sort(p.begin(), p.end());
    int ans = 0;
    int p1 = 0;
    int p2 = p.size() - 1;
    while (p1 <= p2) {
        if (p1 == p2) {
            if (p[p1] <= x) ans++;
            break;
        }
        int g = p[p1];
        int bound = x - g;
        while (p[p2] > bound and p2 > p1) {
            ans++;
            p2--;
        }
        ans++;
        p2--;
        p1++;
    }
    std::cout << ans << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

inline ll read(){
	ll t = 0;
	char c;
	while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
		t=t*10+c-48;
	}
	return t;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n = read();
    int k = read();
    vector<ll> v(n);
    for (int i = 0; i < n; i++) {
        v[i] = read();
    }
    auto can = [&v](ll val, int k) {
        ll sum = 0;
        int count = 1;
        for (int i = 0; i < v.size(); i++) {
            if (sum + v[i] <= val) {
                sum += v[i];
            } else {
                sum = v[i];
                if (sum > val or ++count > k) return false;
            }
        }
        return true;
    };
    ll lo = 0;
    ll hi = 2e14;
    while (lo <= hi) {
        ll mid = lo + (hi - lo) / 2;
        if (can(mid, k)) {
            if (lo == hi) {
                std::cout << mid << std::endl;
                return 0;
            }
            hi = mid;
        } else {
            lo = mid + 1;
        }
    }
    return 0;
}

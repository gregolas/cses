#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>
#include <ext/pb_ds/assoc_container.hpp>

using namespace std;

using ll = long long;
using ull = unsigned long long;

struct data_t {
    int time;
    bool is_start;
    int id;
    data_t(int t, bool start, int i) : time(t), is_start(start), id(i) {}
};

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    vector<data_t> v;
    for (int i = 0; i < n; i++) {
        int a, b;
        cin >> a >> b;
        v.push_back(data_t(a, true, i));
        v.push_back(data_t(b, false, i));
    }
    std::sort(v.begin(), v.end(), [](data_t &a, data_t &b) {
        if (a.time == b.time) {
            if (a.is_start == b.is_start) return a.id < b.id;
            return a.is_start > b.is_start;
        }
        return a.time < b.time;
    });
    set<int> available;
    unordered_map<int, int> occupied;
    int count = 0;
    int ans = 0;
    vector<int> rooms(n);
    for (int i = 0; i < v.size(); i++) {
        if (v[i].is_start) {
            count++;
            int room = 0;
            if (available.empty()) {
                room = count;
            } else {
                room = *available.begin();
                available.erase(available.begin());
            }
            rooms[v[i].id] = room;
            occupied[v[i].id] = room;
            ans = std::max(ans, count);
        } else {
            count--;
            int room = occupied[v[i].id];
            available.insert(room);
            occupied.erase(v[i].id);
        }
    }
    std::cout << ans << '\n';
    for (auto room : rooms) {
        std::cout << room << ' ';
    }
    std::cout << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, x;
    cin >> n >> x;
    vector<pair<long, int>> v(n);
    for (int i = 0; i < n; i++) {
        cin >> v[i].first;
        v[i].second = i + 1;
    }
    sort(v.begin(), v.end());
    int N = v.size();
    for (int i = 0; i < N - 3; i++) {
        for (int j = N - 1; j > i + 2; j--) {
            long sum = v[i].first + v[j].first;
            long target = x - sum;
            int p1 = i + 1;
            int p2 = j - 1;
            while (p1 < p2) {
                long sum2 = v[p1].first + v[p2].first;
                if (sum2 == target) {
                    std::cout << v[i].second << " " << v[j].second << " " << v[p1].second << " " << v[p2].second << '\n';
                    return 0;
                } else if (sum2 > target) {
                    p2--;
                } else {
                    p1++;
                }
            }
        }
    }
    std::cout << "IMPOSSIBLE\n";
    return 0;
}

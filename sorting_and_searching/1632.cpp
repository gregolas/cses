#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, k;
    cin >> n >> k;
    struct data {
        int time;
        bool is_start;
        int id;
        int end;
    };
    vector<data> v(n * 2);
    for (int i = 0; i < n * 2; i += 2) {
        cin >> v[i].time;
        v[i].is_start = true;
        v[i].id = i / 2;
        cin >> v[i + 1].time;
        v[i + 1].is_start = false;
        v[i + 1].id = i / 2;
        v[i].end = v[i + 1].time;
    }
    sort(v.begin(), v.end(), [](auto &a, auto &b) {
        return std::tie(a.time, a.is_start) < std::tie(b.time, b.is_start);
    });
    // end, id
    set<pair<int, int>> s;
    int count = 0;
    for (int i = 0; i < n * 2; i++) {
        if (v[i].is_start) {
            s.insert(make_pair(v[i].end, v[i].id));
            if (s.size() > k) {
                s.erase(prev(s.end()));
            }
        } else {
            auto p = make_pair(v[i].time, v[i].id);
            if (auto it = s.find(p); it != s.end()) {
                s.erase(it);
                count++;
            }
        }
    }
    std::cout << count << '\n';
    return 0;
}

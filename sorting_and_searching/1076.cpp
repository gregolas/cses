#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, k;
    cin >> n >> k;
    vector<int> v(n);
    for (int i = 0; i < n; i++) {
        cin >> v[i];
    }
    multiset<int> below;
    int median;
    multiset<int> above;
    auto remove = [&below, &above, &median](int remove) {
        if (remove == median) return;
        if (remove < median) {
            below.erase(below.find(remove));
            below.insert(median);
        } else {
            above.erase(above.find(remove));
            above.insert(median);
        }
    };
    auto add = [&below, &above, &median](int add) {
        if (add <= *above.begin() and add >= *below.rbegin() ) {
            median = add;
        } else if (add > *above.begin()) {
            median = *above.begin();
            above.erase(above.begin());
            above.insert(add);
        } else {
            median = *below.rbegin();
            below.erase(prev(below.end()));
            below.insert(add);
        }
    };
    vector<int> initial;
    for (int i = 0; i < k; i++) {
        initial.push_back(v[i]);
    }
    sort(initial.begin(), initial.end());
    int idx = k / 2;
    if (k % 2 == 0) idx--;
    median = initial[idx];
    for (int i = 0; i < idx; i++) {
        below.insert(initial[i]);
    }
    for (int i = idx + 1; i < k; i++) {
        above.insert(initial[i]);
    }
    std::cout << median << " ";
    for (int i = k; i < n; i++) {
        if (k == 1) {
            std::cout << v[i] << " ";
            continue;
        } else if (k == 2) {
            std::cout << std::min(v[i], v[i - 1]) << " ";
            continue;
        }
        remove(v[i - k]);
        add(v[i]);
        std::cout << median << " ";
    }
    return 0;
}

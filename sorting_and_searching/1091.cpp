#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;
    multiset<int> tickets;
    for (int i = 0; i < n; i++) {
        int t;
        cin >> t;
        tickets.insert(t);
    }
    for (int i = 0; i < m; i++) {
        int p;
        cin >> p;
        auto it = tickets.upper_bound(p);
        if (it == tickets.begin()) {
            std::cout << "-1\n";
        } else {
            it = std::prev(it);
            std::cout << *it << '\n';
            tickets.erase(it);
        }
    }
    return 0;
}

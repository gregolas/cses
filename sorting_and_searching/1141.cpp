#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>
#include <ext/pb_ds/assoc_container.hpp>
#include <chrono>

using namespace std;

using ll = long long;
using ull = unsigned long long;
    int RANDOM = chrono::high_resolution_clock::now().time_since_epoch().count();
    struct chash {
        int operator()(int x) const { return (x ^ RANDOM) % int(1e9 + 7); }
    };

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    vector<int> songs(n);
    for (int i = 0; i < n; i++) {
        cin >> songs[i];
    }
    map<int, int> pos;
    int start = 0;
    int ans = 0;
    for (int i = 0; i < songs.size(); i++) {
        auto it = pos.find(songs[i]);
        if (it == pos.end() or it->second < start) {
            ans = i - start + 1 > ans ? i - start + 1 : ans;
        } else {
            start = it->second + 1;
        }
        if (it == pos.end()) {
            pos[songs[i]] = i;
        } else {
            it->second = i;
        }
    }
    std::cout << ans << '\n';
    return 0;
}

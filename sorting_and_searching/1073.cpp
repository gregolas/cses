#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <deque>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    vector<int> v(n);
    for (int i = 0; i < n; i++) {
        cin >> v[i];
    }
    deque<int> towers;
    for (int i = n - 1; i >= 0; i--) {
        int val = v[i];
        auto it = std::lower_bound(towers.begin(), towers.end(), val);
        if (it == towers.begin()) {
            towers.push_front(val);
        } else {
            towers[it - towers.begin() - 1] = val;
        }
    }
    std::cout << towers.size() << '\n';
    return 0;
}

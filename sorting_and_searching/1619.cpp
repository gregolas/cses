#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>
#include <cassert>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    vector<pair<int, bool>> customers;
    for (int i = 0; i < n; i++) {
        int a, b;
        cin >> a >> b;
        customers.push_back(make_pair(a, true));
        customers.push_back(make_pair(b, false));
    }
    std::sort(customers.begin(), customers.end(), [](auto &a, auto &b) { return a.first < b.first; });
    int ans = 1;
    int count = 0;
    for (const auto &p : customers) {
        if (p.second) count++;
        else count--;
        ans = std::max(count, ans);
    }
    std::cout << ans << '\n';
    return 0;
}

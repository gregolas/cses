#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    vector<int> v(n);
    for (int i = 0; i < n; i++) {
        cin >> v[i];
    }
    sort(v.begin(), v.end());
    int mid_right = v.size() / 2;
    int mid_left = mid_right - (v.size() % 2 == 0);
    long sum_left = 0;
    for (int i = 0; i < mid_left; i++) {
        sum_left += v[mid_left] - v[i];
    }
    long sum_right = 0;
    for (int i = mid_right + 1; i < v.size(); i++) {
        sum_right += v[i] - v[mid_right];
    }
    long ans = sum_left + sum_right;
    if (v[mid_left] != v[mid_right]) {
        ans += (v[mid_right] - v[mid_left]) * n / 2;
    }
    cout << ans << '\n';
    return 0;
}

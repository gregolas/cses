#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

inline int read(){
	int t = 0;
	char c;
	while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
		t=t*10+c-48;
	}
	return t;
}

bool can(unsigned long n, unsigned long t, const vector<int> &v) {
    for (auto &k : v) {
        if (n / k >= t) return true;
        t -= n / k;
    }
    return false;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n = read();
    int t = read();
    vector<int> v(n);
    for (int i = 0; i < n; i++) v[i] = read();
    unsigned long lo = 0;
    unsigned long hi = 1e18;
    unsigned long mid;
    while (lo <= hi) {
        mid = lo + (hi - lo) / 2;
        if (lo == hi) break;
        if (can(mid, t, v)) hi = mid;
        else lo = mid + 1;
    }
    std::cout << mid << '\n';
    return 0;
}

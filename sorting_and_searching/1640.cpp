#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, x;
    cin >> n >> x;
    vector<pair<int, int>> v(n);
    for (int i = 0; i < n; i++) {
        cin >> v[i].first;
        v[i].second = i + 1;
    }
    sort(v.begin(), v.end());
    int p1 = 0;
    int p2 = v.size() - 1;
    while (p1 < p2) {
        int sum = v[p1].first + v[p2].first;
        if (sum == x) {
            int i1 = v[p1].second;
            int i2 = v[p2].second;
            std::cout << std::min(i1, i2) << " " << std::max(i1, i2) << '\n';
            return 0;
        } else if (sum < x) {
            p1++;
        } else {
            p2--;
        }
    }
    std::cout << "IMPOSSIBLE\n";
    return 0;
}

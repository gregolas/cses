#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    vector<pair<int, int>> v(n);
    int sum = 0;
    for (int i = 0; i < n; i++) {
        cin >> v[i].first;
        cin >> v[i].second;
        sum += v[i].first;
    }
    vector<pair<int, int>> v2;
    for (int i = 0; i < v.size(); i++) {
        int val = v[i].second - v[i].first;
        val += sum - v[i].second;
        v2.push_back(make_pair(val, i));
    }
    sort(v2.begin(), v2.end(), std::greater<pair<int, int>>{});
    long pos = 0;
    long ans = 0;
    for (int i = 0; i < v2.size(); i++) {
        int idx = v2[i].second;
        pos += v[idx].first;
        long reward = v[idx].second - pos;
        ans += reward;
    }
    std::cout << ans << '\n';
    return 0;
}

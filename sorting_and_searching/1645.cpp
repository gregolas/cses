#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    vector<pair<int, int>> v(n);
    for (int i = 0; i < n; i++) {
        cin >> v[i].first;
        v[i].second = i + 1;
    }
    sort(v.begin(), v.end());
    set<int> s;
    vector<int> ans(n);
    set<int> sn;
    int prev = -1;
    for (int i = 0; i < v.size(); i++) {
        auto val = v[i].first;
        if (val != prev) {
            s.insert(sn.begin(), sn.end());
            sn.clear();
        }
        auto pos = v[i].second;
        auto it = s.lower_bound(pos);
        if (it == s.begin()) {
            ans[pos - 1] = 0;
        } else {
            it = std::prev(it);
            ans[pos - 1] = *it;
        }
        prev = val;
        sn.insert(pos);
    }
    for (auto v : ans) {
        std::cout << v << " ";
    }
    std::cout << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m, k;
    cin >> n >> m >> k;
    vector<int> desired(n);
    vector<bool> used(n);
    vector<int> apt(m);
    for (int i = 0; i < n; i++) {
        cin >> desired[i];
    }
    for (int i = 0; i < m; i++) {
        cin >> apt[i];
    }
    std::sort(desired.begin(), desired.end());
    std::sort(apt.begin(), apt.end());
    int start = 0;
    int ans = 0;
    for (int i = 0; i < apt.size(); i++) {
        int a = apt[i];
        auto lower = std::lower_bound(desired.begin() + start, desired.end(), a - k);
        auto upper = std::upper_bound(desired.begin() + start, desired.end(), a + k);
        upper = std::prev(upper);
        while (lower <= upper) {
            if (not used[lower - desired.begin()] and std::abs(*lower - a) <= k) {
                used[lower - desired.begin()] = true;
                ans++;
                start = lower - desired.begin();
                break;
            }
            lower++;
        }
    }
    std::cout << ans << '\n';
    return 0;
}

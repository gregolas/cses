#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    // time, is_start
    struct movie {
        int time;
        bool is_start;
        int id;
        movie(int t, bool s, int i) : time(t), is_start(s), id(i) {}
    };
    vector<movie> movies;
    for (int i = 0; i < n; i++) {
        int a, b;
        cin >> a >> b;
        movies.push_back(movie(a, true, i));
        movies.push_back(movie(b, false, i));
    }
    std::sort(movies.begin(), movies.end(), [](auto &a, auto &b) {
        if (a.time == b.time) {
            if (a.is_start == b.is_start) {
                return a.id < b.id;
            } else {
                return not a.is_start;
            }
        }
        return a.time < b.time;
    });
    int count = 0;
    unordered_set<int> open;
    for (int i = 0; i < movies.size(); i++) {
        auto movie = movies[i];
        if (movie.is_start) {
            open.insert(movie.id);
        } else {
            if (open.count(movie.id)) {
                count++;
                open.clear();
            }
        }
    }
    std::cout << count << '\n';
    return 0;
}

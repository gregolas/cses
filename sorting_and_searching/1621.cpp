#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    vector<bool> s(1e9 + 1);
    int count = 0;
    for (int i = 0; i < n; i++) {
        int c;
        cin >> c;
        if (not s[c]) {
            count++;
        }
        s[c] = true;
    }
    std::cout << count << '\n';
    return 0;
}

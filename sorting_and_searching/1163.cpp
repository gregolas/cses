#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int x, n;
    cin >> x >> n;
    multiset<int> segments;
    set<int> lights;
    lights.insert(0);
    lights.insert(x);
    segments.insert(x);
    for (int i = 0; i < n; i++) {
        int light;
        cin >> light;
        auto it = lights.upper_bound(light);
        int end = *it;
        int start = *std::prev(it);
        int size = end - start;
        segments.erase(segments.find(size));
        segments.insert(light - start);
        segments.insert(end - light);
        lights.insert(light);
        std::cout << *segments.rbegin() << ' ';
    }
    std::cout << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

inline ll read() {
	int t = 0;
	char c;
    bool is_neg = false;
	while ((c = getchar_unlocked()) >= 45) {
        switch (c) {
            case '-':
                is_neg = true;
                break;
            default:
		        t=t*10+c-48;
        }
	}
	return is_neg ? -t : t;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, a, b;
    cin >> n >> a >> b;
    vector<ll> sums(n + 1);
    ll sum = 0;
    multiset<ll> s;
    ll ans = numeric_limits<ll>::min();
    for (int i = 0; i <= n; i++) {
        if (i) {
            ll val;
            cin >> val;
            sum += val;
            sums[i] = sum;
        }
        int min = i - b - 1;
        int max = i - a;
        if (min >= 0) s.erase(s.find(sums[min]));
        if (max >= 0) s.insert(sums[max]);
        if (not s.empty())
            ans = std::max(ans, sums[i] - *s.begin());
    }
    std::cout << ans << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>

using namespace std;

using ll = long long;

struct edge {
    int from, to;
    ll weight = -1;
    edge(int f, int t) : from(f), to(t) {}
};

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;
    unordered_map<int, vector<int>> graph;
    vector<edge> edges;
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        graph[a].push_back(b);
        edges.push_back(edge(a, b));
    }
    vector<ll> distances(n + 1, numeric_limits<ll>::max());
    vector<int> parents(n + 1, -1);
    distances[1] = 0;
    for (int i = 0; i < n - 1; i++) {
        bool changed = false;
        for (const auto &e : edges) {
            if (distances[e.from] != numeric_limits<ll>::max() and distances[e.from] + e.weight < distances[e.to]) {
                distances[e.to] = distances[e.from] + e.weight;
                parents[e.to] = e.from;
                changed = true;
            }
        }
        if (not changed) break;
    }
    if (distances.back() == numeric_limits<ll>::max()) {
        std::cout << "IMPOSSIBLE\n";
        return 0;
    }
    int curr = n;
    stack<int> path;
    while (curr != 1) {
        path.push(curr);
        curr = parents[curr];
    }
    path.push(1);
    std::cout << path.size() << '\n';
    while (not path.empty()) {
        std::cout << path.top() << ' ';
        path.pop();
    }
    std::cout << '\n';
    return 0;
}

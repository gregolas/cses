#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

typedef unsigned long long ull;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m, q;
    cin >> n >> m >> q;
    vector<vector<ull>> graph(n + 1, vector<ull>(n + 1, std::numeric_limits<long long>::max()));
    for (int i = 0; i < m; i++) {
        ull a, b, c;
        cin >> a >> b >> c;
        graph[a][b] = std::min(graph[a][b], c);
        graph[b][a] = std::min(graph[a][b], c);
    }
    for (int j = 1; j <= n; j++) {
        for (int i = 1; i <= n; i++) {
            for (int k = 1; k <= n; k++) {
                if (graph[i][j] + graph[j][k] < graph[i][k]) {
                    graph[i][k] = graph[i][j] + graph[j][k];
                }
            }
        }
    }
    for (int i = 0; i < q; i++) {
        int a, b;
        cin >> a >> b;
        if (a == b) std::cout << "0" << std::endl;
        else if (graph[a][b] == std::numeric_limits<long long>::max()) {
            cout << -1 << '\n';
        } else {
            cout << graph[a][b] << '\n';
        }
    }
    return 0;
}

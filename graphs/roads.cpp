#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <utility>

using namespace std;

void dfs(int node, unordered_map<int, vector<int>> &graph, vector<bool> &seen) {
    for (int n : graph[node]) {
        if (not seen[n]) {
            seen[n] = true;
            dfs(n, graph, seen);
        }
    }
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;
    unordered_map<int, vector<int>> graph;
    vector<bool> seen(n);
    for (int i = 0; i < m; i++) {
        int from, to;
        cin >> from >> to;
        graph[from].push_back(to);
        graph[to].push_back(from);
    }
    vector<int> groups;
    for (int i = 1; i <= n; i++) {
        if (not seen[i]) {
            seen[i] = true;
            groups.push_back(i);
            dfs(i, graph, seen);
        }
    }
    int needed = groups.size() - 1;
    std::cout << needed << '\n';
    for (int i = 1; i < groups.size(); i++) {
        std::cout << groups[i - 1] << " " << groups[i] << '\n';
    }
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>

using namespace std;

int find_cycle(int n, unordered_map<int, vector<int>> &graph, unordered_set<int> &seen, unordered_set<int> &path, unordered_map<int, int> &parent) {
    if (path.count(n)) {
        return n;
    }
    if (seen.count(n)) {
        return -1;
    }
    path.insert(n);
    seen.insert(n);
    for (auto neighbor : graph[n]) {
        parent[neighbor] = n;
        int res = find_cycle(neighbor, graph, seen, path, parent);
        if (res != -1) {
            return res;
        }
    }
    path.erase(n);
    return -1;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;
    unordered_map<int, vector<int>> graph;
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        graph[a].push_back(b);
    }
    unordered_set<int> seen;
    for (int i = 1; i <= n; i++) {
        if (not seen.count(i)) {
            unordered_set<int> path;
            unordered_map<int, int> parent;
            int cycle_head = find_cycle(i, graph, seen, path, parent);
            if (cycle_head != -1) {
                int curr = cycle_head;
                stack<int> s;
                do {
                    s.push(curr);
                    curr = parent[curr];
                } while (curr != cycle_head);
                s.push(curr);
                std::cout << s.size() << '\n';
                while (not s.empty()) {
                    std::cout << s.top() << " ";
                    s.pop();
                }
                std::cout << '\n';
                return 0;
            }
        }
    }
    std::cout << "IMPOSSIBLE" << std::endl;
    return 0;
}

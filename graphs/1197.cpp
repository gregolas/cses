#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <utility>

using namespace std;

typedef long long ll;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;
    struct edge {
        ll from, to, weight;
        edge(ll f, ll t, ll w) : from(f), to(t), weight(w) {}
    };
    vector<edge> edges;
    for (int i = 0; i < m; i++) {
        ll a, b, c;
        cin >> a >> b >> c;
        edges.push_back(edge(a, b, c));
    }
    vector<ll> distances(n + 1, 1e9);
    vector<int> parent(n + 1, 1e9);
    distances[1] = 0;
    parent[1] = 0;
    for (int i = 0; i < n - 1; i++) {
        for (const auto &edge : edges) {
            if (distances[edge.from] + edge.weight < distances[edge.to]) {
                distances[edge.to] = distances[edge.from] + edge.weight;
                parent[edge.to] = edge.from;
            }
        }
    }
    int curr = -1;
    for (const auto &edge : edges) {
        if (distances[edge.from] + edge.weight < distances[edge.to]) {
            curr = edge.to;
            distances[edge.to] = distances[edge.from] + edge.weight;
            parent[edge.to] = edge.from;
        }
    }
    if (curr == -1) {
        std::cout << "NO\n";
        return 0;
    }
    std::cout << "YES\n";
    vector<bool> seen(n + 1);
    vector<int> path;

    while (not seen[curr]) {
        seen[curr] = true;
        path.push_back(curr);
        curr = parent[curr];
    }
    path.push_back(curr);
    int cnt = 0;
    for (int j = path.size() - 1; j >= 0; j--) {
        int i = path[j];
        if (i == curr) cnt += 1;
        if (cnt >= 1)
            std::cout << i << " ";
        if (cnt == 2) break;
    }
    std::cout << '\n';
    return 0;
}

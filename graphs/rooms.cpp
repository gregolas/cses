#include <iostream>
#include <vector>
#include <map>
#include <functional>

using namespace std;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    std::cin >> n >> m;
    vector<string> grid(n);
    for (int i = 0; i < n; i++) {
        std::cin >> grid[i];
    }
    vector<vector<bool>> seen(n, vector<bool>(m));
    int rooms = 0;
    std::function<void(int, int)> go;
    go = [&seen, &grid, &n, &m, &go](int i, int j) {
        if (i < 0 or i >= n or j < 0 or j >= m or seen[i][j] or grid[i][j] == '#') return;
        seen[i][j] = true;
        go(i + 1, j);
        go(i - 1, j);
        go(i, j + 1);
        go(i, j - 1);
    };
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (not seen[i][j] and grid[i][j] == '.') {
                rooms++;
                go(i, j);
            }
        }
    }
    std::cout << rooms << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, q;
    cin >> n >> q;
    vector<int> planets(n + 1);
    vector<vector<int>> path(n + 1, vector<int>(32));
    for (int i = 0; i < n; i++) {
        cin >> planets[i + 1];
    }
    int val = 1;
    for (int i = 1; i <= 26; i++) {
        for (int j = 1; j <= n; j++) {
            if (val == 1) {
                path[j][i] = planets[j];
            } else {
                path[j][i] = path[path[j][i - 1]][i - 1];
            }
        }
        val <<= 1;
    }
    for (int i = 0; i < q; i++) {
        int node;
        cin >> node;
        int query;
        cin >> query;
        for (int j = 27; j >= 0; j--) {
            int val = (1 << j);
            if (val <= query) {
                node = path[node][j + 1];
                query -= val;
            }
            if (query == 0) break;
        }
        cout << node << '\n';
    }
    return 0;
}

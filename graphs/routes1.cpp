#include <iostream>
#include <vector>
#include <limits>
#include <map>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <utility>
#include <queue>
#include <functional>

using namespace std;

typedef long long ull;

bool cmp(const pair<ull, ull> &a, const pair<ull, ull> &b) {
    return a.second > b.second;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    ull n, m;
    cin >> n >> m;
    unordered_map<ull, vector<pair<ull, ull>>> graph;
    vector<ull> distances(n + 1, std::numeric_limits<long long>::max());
    for (int i = 0; i < m; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        graph[a].push_back(make_pair(b, c));
    }
    priority_queue<pair<ull, ull>, vector<pair<ull, ull>>, std::function<bool(const pair<ull, ull> &, const pair<ull, ull> &)>> q(cmp);
    q.push(make_pair(1, 0));
    distances[1] = 0;
    vector<int> seen(n + 1, false);
    while (not q.empty()) {
        auto t = q.top();
        q.pop();
        auto from = t.first;
        auto dist = t.second;
        if (seen[from]) continue;
        seen[from] = true;
        for (auto neighbor : graph[from]) {
            auto to = neighbor.first;
            auto from_to_dist = neighbor.second;
            if (distances[from] + neighbor.second < distances[to]) {
                distances[to] = distances[from] + from_to_dist;
                q.push(make_pair(neighbor.first, distances[to]));
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        std::cout << distances[i] <<  ' ';
    }
    std::cout << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>

using namespace std;

bool dfs(int node, vector<int> &state, unordered_map<int, vector<int>> &graph, vector<int> &order) {
    if (state[node] == 1) return false;
    if (state[node] == 2) return true;
    bool res = true;
    state[node] = 1;
    for (auto neighbor : graph[node]) {
        res &= dfs(neighbor, state, graph, order);
    }

    state[node] = 2;
    order.push_back(node);
    return res;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;
    unordered_map<int, vector<int>> graph;
    vector<int> state(n + 1);
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        graph[a].push_back(b);
    }
    vector<int> order;
    for (int i = 1; i <= n; i++) {
        if (state[i] == 2) {
            continue;
        }
        if (not dfs(i, state, graph, order)) {
            std::cout << "IMPOSSIBLE\n";
            return 0;
        }
    }
    for (int i = order.size() - 1; i >= 0; i--) {
        std::cout << order[i] << ' ';
    }
    std::cout << '\n';
    return 0;
}

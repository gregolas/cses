#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <utility>
#include <queue>

using namespace std;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;
    unordered_map<int, vector<int>> graph;
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        graph[a].push_back(b);
        graph[b].push_back(a);
    }
    vector<int> colors(n + 1);
    vector<bool> seen(n + 1);
    queue<int> q;
    for (int i = 1; i <= n; i++) {
        if (seen[i]) continue;
        q.push(i);
        seen[i] = true;
        colors[i] = 1;
        while (not q.empty()) {
            auto f = q.front();
            int new_color = colors[f] == 1 ? 2 : 1;
            q.pop();
            for (auto n : graph[f]) {
                if (not seen[n]) {
                    seen[n] = true;
                    q.push(n);
                    colors[n] = new_color;
                } else if (colors[n] == colors[f]) {
                    std::cout << "IMPOSSIBLE\n";
                    return 0;
                }
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        std::cout << colors[i] << ' ';
    }
    std::cout << '\n';
    return 0;
}

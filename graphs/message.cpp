#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <utility>
#include <queue>
#include <stack>

using namespace std;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;
    unordered_map<int, vector<int>> graph;
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        graph[a].push_back(b);
        graph[b].push_back(a);
    }
    vector<int> parent(n + 1, -1);
    vector<bool> seen(n + 1, false);
    queue<int> q;
    q.push(1);
    seen[1] = true;
    while (not q.empty()) {
        auto f = q.front();
        q.pop();
        for (auto n : graph[f]) {
            if (not seen[n]) {
                seen[n] = true;
                q.push(n);
                parent[n] = f;
            }
        }
    }
    if (not seen.back()) {
        std::cout << "IMPOSSIBLE\n";
    } else {
        stack<int> path;
        path.push(n);
        auto curr = parent.back();
        while (curr != -1) {
            path.push(curr);
            curr = parent[curr];
        }
        std::cout << path.size() << '\n';
        while (not path.empty()) {
            std::cout << path.top() << " ";
            path.pop();
        }
        std::cout << '\n';
    }
    return 0;
}

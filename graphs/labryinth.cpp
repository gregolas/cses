#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <utility>
#include <queue>

using namespace std;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;
    vector<vector<char>> grid(n, vector<char>(m));
    vector<vector<bool>> seen(n, vector<bool>(m));
    vector<vector<pair<int, int>>> parent(n, vector<pair<int, int>>(m));
    vector<vector<char>> move(n, vector<char>(m));
    int starti, startj;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> grid[i][j];
            if (grid[i][j] == 'A') {
                starti = i;
                startj = j;
            }
        }
    }
    
    queue<tuple<int, int, char, pair<int, int>>> q;
    q.push(make_tuple(starti, startj, 'x', make_pair(-1, -1)));
    seen[starti][startj] = true;
    while (not q.empty()) {
        auto f = q.front();
        q.pop();

        int curri = std::get<0>(f);
        int currj = std::get<1>(f);
        char currmove  = std::get<2>(f);
        pair<int, int> currparent  = std::get<3>(f);
        move[curri][currj] = currmove;
        parent[curri][currj] = currparent;

        if (grid[curri][currj] == 'B') {
            auto curr = make_pair(curri, currj);
            std::string res;
            while (curr.first != -1) {
                res += move[curr.first][curr.second];
                curr = parent[curr.first][curr.second];
            }
            std::reverse(res.begin(), res.end());
            std::cout << "YES\n" << res.size() - 1 << '\n' << res.substr(1) << '\n';
            return 0;
        }

        auto ok = [&grid, &seen](int i, int j) {
            return i >= 0 and i < grid.size() and j >= 0 and j < grid[0].size() and grid[i][j] != '#' and not seen[i][j];
        };

        if (ok(curri + 1, currj)) {
            seen[curri + 1][currj] = true;
            q.push(make_tuple(curri + 1, currj, 'D', make_pair(curri, currj)));
        }
        if (ok(curri - 1, currj)) {
            seen[curri - 1][currj] = true;
            q.push(make_tuple(curri - 1, currj, 'U', make_pair(curri, currj)));
        }
        if (ok(curri, currj + 1)) {
            seen[curri][currj + 1] = true;
            q.push(make_tuple(curri, currj + 1, 'R', make_pair(curri, currj)));
        }
        if (ok(curri, currj - 1)) {
            seen[curri][currj - 1] = true;
            q.push(make_tuple(curri, currj - 1, 'L', make_pair(curri, currj)));
        }
    }
    std::cout << "NO\n";
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <queue>
#include <stack>
#include <random>

using namespace std;

int n, m;
vector<vector<char>> graph;
vector<vector<int>> monster_distances;

void bfs(const pair<int, int> &start) {
    queue<pair<int, int>> q;
    queue<pair<int, int>> q2;
    q.push(start);
    int distance = 0;
    while (not q.empty()) {
        while (not q.empty()) {
            auto f = q.front();
            q.pop();

            auto ok = [&graph = graph, &d = monster_distances](int i, int j, int dist) {
                if (i < 0 or i >= static_cast<int>(graph.size())) return false;
                if (j < 0 or j >= static_cast<int>(graph[0].size())) return false;
                if (graph[i][j] == '#' or graph[i][j] == 'M') return false;
                if (d[i][j] <= dist + 1) return false;
                return true;
            };
            int curri = f.first;
            int currj = f.second;
            if (monster_distances[curri][currj] < distance) continue;
            monster_distances[curri][currj] = distance;
            auto test = [&ok, &q2, &distances = monster_distances](int i, int j, int dist) {
                if (ok(i, j, dist)) {
                    distances[i][j] = dist + 1;
                    q2.push(make_pair(i, j));
                }
            };
            test(curri + 1, currj, distance);
            test(curri - 1, currj, distance);
            test(curri, currj + 1, distance);
            test(curri, currj - 1, distance);
        }
        distance++;
        std::swap(q, q2);
    }
}


int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m;
    graph.resize(n, vector<char>(m));
    monster_distances.resize(n, vector<int>(m, 1e9));
    int starti, startj;
    vector<pair<int, int>> monsters;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> graph[i][j];
            if (graph[i][j] == 'A') {
                starti = i; startj = j;
            } else if (graph[i][j] == 'M') {
                monsters.push_back(make_pair(i, j));
            }
        }
    }
    auto rng = std::default_random_engine{};
    std::shuffle(monsters.begin(), monsters.end(), rng);
    for (const auto &monster : monsters) {
        bfs(monster);
    }

    vector<vector<bool>> seen(n, vector<bool>(m));
    vector<vector<pair<int, int>>> parents(n, vector<pair<int, int>>(m, make_pair(-1, -1)));
    queue<pair<int, int>> q;
    queue<pair<int, int>> q2;
    q.push(make_pair(starti, startj));
    seen[starti][startj] = true;
    parents[starti][startj] = make_pair(-1, -1);
    int distance = 0;
    while (not q.empty()) {
        while (not q.empty()) {
            auto f = q.front();
            q.pop();
            int curri = f.first;
            int currj = f.second;
            if (distance < monster_distances[curri][currj]) {
                auto ok = [&graph = graph, &seen](int i, int j) {
                    if (i < 0 or i >= static_cast<int>(graph.size()) or j < 0 or j >= static_cast<int>(graph[0].size())) return false;
                    if (seen[i][j]) return false;
                    if (graph[i][j] == '#') return false;
                    return true;
                };
                auto test = [&ok, &q2, &seen, &parents, curri, currj](int i, int j) {
                    if (ok(i, j)) {
                        seen[i][j] = true;
                        parents[i][j] = make_pair(curri, currj);
                        q2.push(make_pair(i, j));
                    }
                };
                test(curri + 1, currj);
                test(curri - 1, currj);
                test(curri, currj + 1);
                test(curri, currj - 1);
                if (curri == 0 or curri == n - 1 or currj == 0 or currj == m - 1) {
                    std::cout << "YES\n" << distance << std::endl;
                    auto curr = f;
                    stack<char> s;
                    auto direction = [](int i, int j, int ii, int jj) {
                        if (i > ii) return 'D';
                        else if (i < ii) return 'U';
                        else if (j > jj) return 'R';
                        else if (j < jj) return 'L';
                        return 'X';
                    };
                    while (curr != make_pair(-1, -1)) {
                        auto next = parents[curr.first][curr.second];
                        if (next == make_pair(-1, -1)) break;
                        s.push(direction(curr.first, curr.second, next.first, next.second));
                        curr = next;
                    }
                    while (not s.empty()) {
                        std::cout << s.top();
                        s.pop();
                    }
                    std::cout << '\n';
                    return 0;
                }
            } else {
            }
        }
        std::swap(q, q2);
        distance++;
    }
    std::cout << "NO" << std::endl;

    return 0;
}

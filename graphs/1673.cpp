#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;
template <typename K, typename V>
using umap = unordered_map<K, V>;

typedef long long ll;

bool dfs(int node, int target, umap<int, vector<int>> &graph, vector<bool> &seen) {
    if (seen[node]) return false;
    if (node == target) return true;
    int res = false;
    seen[node] = true;
    for (auto next : graph[node]) {
        res |= dfs(next, target, graph, seen);
    }
    return res;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;

    struct edge {
        int from, to;
        ll weight;
        edge(int f, int t, ll w) : from(f), to(t), weight(w) {}
    };
    unordered_map<int, vector<int>> graph;
    vector<edge> edges;
    for (int i = 0; i < m; i++) {
        int a, b;
        ll c;
        cin >> a >> b >> c;
        graph[a].push_back(b);
        edges.emplace_back(a, b, -c);
    }
    constexpr ll MAX = numeric_limits<ll>::max();
    vector<ll> distance(n + 1, MAX);
    vector<int> parent(n + 1);
    distance[1] = 0;
    for (int i = 0; i < n - 1; i++) {
        for (const edge &e : edges) {
            if (distance[e.from] != MAX) {
                distance[e.to] = std::min(distance[e.to], distance[e.from] + e.weight);
                parent[e.to] = e.from;
            }
        }
    }
    int found = -1;
    for (const edge &e : edges) {
        if (distance[e.to] > distance[e.from] + e.weight) {
            parent[e.to] = e.from;
            found = e.to;
            break;
        }
    }

    if (found == -1) {
        std::cout << -distance.back() << '\n';
        return 0;
    }

    vector<bool> seen(n + 1);
    int curr = found;
    while (not seen[curr]) {
        seen[curr] = true;
        curr = parent[curr];
    }

    std::fill(seen.begin(), seen.end(), false);
    if (dfs(curr, n, graph, seen)) {
        std::fill(seen.begin(), seen.end(), false);
        if (dfs(1, curr, graph, seen)) {
            std::cout << "-1\n";
            return 0;
        }
    }
    std::cout << -distance.back() << '\n';

    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <queue>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>

using namespace std;

template <typename T>
void add(T &a, T b) {
    constexpr int MOD = 1e9 + 7;
    a += b;
    a %= MOD;
}

int dfs(int node, unordered_map<int, vector<int>> &graph, vector<int> &ways, vector<bool> &seen) {
    if (seen[node]) return ways[node];
    int res = 0;
    seen[node] = true;
    if (node == 1) {
        ways[node] = 1;
        return 1;
    }
    for (auto neighbor : graph[node]) {
        add(res, dfs(neighbor, graph, ways, seen));
    }
    ways[node] = res;
    return res;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;
    unordered_map<int, vector<int>> graph;
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        graph[b].push_back(a);
    }
    vector<int> ways(n + 1);
    vector<bool> seen(n + 1);
    dfs(n, graph, ways, seen);
    std::cout << ways.back() << '\n';
    return 0;
}

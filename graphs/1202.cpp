#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

struct cmp {
    bool operator() (const pair<ll, ll> &p1, const pair<ll, ll> &p2) {
        return p1.second > p2.second;
    }
};

template <typename T>
void add(T &a, T b) {
    constexpr ll MOD = 1e9 + 7;
    a += b;
    a %= MOD;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, m;
    cin >> n >> m;
    unordered_map<int, vector<pair<int, int>>> graph;
    for (int i = 0; i < m; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        graph[a].push_back(make_pair(b, c));
    }
    vector<ll> distances(n + 1, numeric_limits<ll>::max());
    distances[1] = 0;
    struct data_t {
        ll ways = 0;
        int min_flights = numeric_limits<int>::max();
        int max_flights = numeric_limits<int>::min();
    };
    vector<data_t> data(n + 1);
    data[1].ways = 1;
    data[1].min_flights = 0;
    data[1].max_flights = 0;
    priority_queue<pair<ll, ll>, vector<pair<ll, ll>>, cmp> pq;
    pq.push(make_pair(1, 0));
    vector<bool> seen(n + 1);
    while (not pq.empty()) {
        auto f = pq.top();
        pq.pop();

        if (seen[f.first]) continue;
        seen[f.first] = true;
        for (auto neighbor : graph[f.first]) {
            if (distances[f.first] + neighbor.second == distances[neighbor.first]) {
                distances[neighbor.first] = distances[f.first] + neighbor.second;
                data[neighbor.first].min_flights = std::min(data[neighbor.first].min_flights, data[f.first].min_flights + 1);
                data[neighbor.first].max_flights = std::max(data[neighbor.first].max_flights, data[f.first].max_flights + 1);
                add(data[neighbor.first].ways, data[f.first].ways);
            } else if (distances[f.first] + neighbor.second < distances[neighbor.first]) {
                distances[neighbor.first] = distances[f.first] + neighbor.second;
                data[neighbor.first].min_flights = data[f.first].min_flights + 1;
                data[neighbor.first].max_flights = data[f.first].max_flights + 1;
                data[neighbor.first].ways = data[f.first].ways;
            }
            pq.push(make_pair(neighbor.first, distances[neighbor.first]));
        }
    }
    std::cout << distances.back() << ' ';
    std::cout << data.back().ways << ' ';
    std::cout << data.back().min_flights << ' ';
    std::cout << data.back().max_flights << '\n';
    return 0;
}

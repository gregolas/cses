#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <stack>
#include <utility>

using namespace std;

int n, m;
unordered_map<int, vector<int>> graph;
vector<int> parent;
vector<bool> seen;
int dfs(int n, int p) {
    parent[n] = p;
    seen[n] = true;
    int res = -1;
    for (auto next : graph[n]) {
        if (next == p) continue;
        if (seen[next]) {
            parent[next] = n;
            return next;
        } else {
            int res2 = dfs(next, n);
            if (res2 != -1) {
                return res2;
            }
        }
    }
    return res;
}

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m;
    seen.resize(n + 1, false);
    parent.resize(n + 1, -1);
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        graph[a].push_back(b);
        graph[b].push_back(a);
    }
    int curr = -1;
    for (int i = 1; i <= n; i++) {
        if (not seen[i]) {
            curr = dfs(i, -1);
            if (curr != -1) break;
        }
    }
    if (curr == -1) {
        std::cout << "IMPOSSIBLE\n";
        return 0;
    }
    int orig = curr;
    stack<int> s;
    do {
        s.push(curr);
        curr = parent[curr];
    } while (curr != orig);
    s.push(orig);
    std::cout << s.size() << std::endl;
    while (not s.empty()) {
        std::cout << s.top() << ' ';
        s.pop();
    }
    return 0;
}

#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n, x;
    cin >> n >> x;

    vector<int> v(n);
    for (size_t i = 0; i < n; i++) {
        cin >> v[i];
    }

    std::sort(v.begin(), v.end());
    int p1 = 0, p2 = n - 1;
    int ans = 0;
    while (p1 <= p2) {
        if (p1 == p2) {
            ans += v[p1] <= x;
            p1++;
        } else {
            if (v[p1] + v[p2] <= x) {
                ans++;
                p1++;
                p2--;
            } else {
                ans++;
                p2--;
            }
        }
    }
    std::cout << ans << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <cassert>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n, m, k;
    cin >> n >> m >> k;
    std::vector<int> a(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }
    std::sort(a.begin(), a.end());
    std::vector<int> b(m);
    for (int i = 0; i < m; i++) {
        cin >> b[i];
    }
    std::sort(b.begin(), b.end());
    int p1 = 0, p2 = 0;
    int ans = 0;
    auto ok = [&k](int des, int size) {
        if (size < des - k) return -1;
        if (size > des + k) return 1;
        return 0;
    };
    while (p1 < a.size() and p2 < b.size()) {
        int res = ok(a[p1], b[p2]);
        if (res == 0) {
            ans++;
            p1++;
            p2++;
        } else if (res == -1) {
            p2++;
        } else if (res == 1) {
            p1++;
        }
    }
    std::cout << ans << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int num;
    cin >> num;
    std::vector<int> v;
    v.resize(num);
    for (int i = 0; i < num; i++) {
        cin >> v[i];
    }
    std::sort(v.begin(), v.end());
    int ans = 1;
    for (int i = 1; i < v.size(); i++) {
        if (v[i] != v[i - 1]) ans++;
    }
    std::cout << ans << '\n';
    return 0;
}

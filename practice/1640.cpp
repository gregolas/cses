#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n, x;
    cin >> n >> x;
    vector<pair<int, int>> v(n);
    for (int i = 0; i < n; i++) {
        cin >> v[i].first;
        v[i].second = i + 1;
    }
    std::sort(v.begin(), v.end());
    int p1 = 0, p2 = n - 1;
    while (p1 < p2) {
        int sum = v[p1].first + v[p2].first;
        if (sum == x) {
            std::cout << v[p1].second << " " << v[p2].second << '\n';
            return 0;
        } else if (sum > x) {
            p2--;
        } else {
            p1++;
        }
    }
    std::cout << "IMPOSSIBLE\n";
    return 0;
}

#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n;
    cin >> n;
    vector<pair<int, int>> times;
    for (int i = 0; i < n; i++) {
        int a, b;
        cin >> a >> b;
        times.emplace_back(a, 1);
        times.emplace_back(b, -1);
    }
    std::sort(times.begin(), times.end());
    int cnt = 0;
    int ans = 0;

    for (int i = 0; i < n * 2; i++) {
        cnt += times[i].second;
        ans = std::max(cnt, ans);
    }
    std::cout << ans << '\n';
    return 0;
}

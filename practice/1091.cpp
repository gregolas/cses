#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n, m;
    cin >> n >> m;
    multiset<int> prices;
    for (size_t i = 0; i < n; i++) {
        int price;
        cin >> price;
        prices.insert(price);
    }
    for (size_t i = 0; i < m; i++) {
        int price;
        cin >> price;
        auto it = prices.upper_bound(price);
        if (it == prices.begin()) {
            std::cout << "-1\n";
        } else {
            it = std::prev(it);
            std::cout << *it << '\n';
            prices.erase(it);
        }
    }
    return 0;
}

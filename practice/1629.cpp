#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n;
    cin >> n;
    vector<pair<int, int>> v(n);
    for (int i = 0; i < n; i++) {
        cin >> v[i].first;
        cin >> v[i].second;
    }
    std::sort(v.begin(), v.end());
    int end = v[0].second;
    int ans = 1;
    for (int i = 1; i < n; i++) {
        if (v[i].second < end) {
            end = v[i].second;
        } else if (v[i].first >= end) {
            ans++;
            end = std::max(end, v[i].second);
        }
    }
    std::cout << ans << '\n';
    return 0;
}

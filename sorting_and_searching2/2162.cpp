#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

constexpr int mod = 1e9 + 7;

template <typename T = int>
inline T read(){
  T t = 0;
  char c;
  while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
    t=t*10+c-48;
  }
  return t;
}

int main(void) {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n = read();
  std::set<int> s;
  for (int i = 1; i <= n; i++) s.insert(i);
  auto it = s.begin();
  while (not s.empty()) {
    it++;
    if (it == s.end()) it = s.begin();
    std::cout << *it << ' ';
    it = s.erase(it);
    if (it == s.end()) it = s.begin();
  }
  return 0;
}

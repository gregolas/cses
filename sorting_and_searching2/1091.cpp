#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

template <typename T = int>
inline T read(){
  T t = 0;
  char c;
  while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
    t=t*10+c-48;
  }
  return t;
}

int main(void) {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n = read();
  int m = read();
  std::multiset<int> h;
  for (int i = 0; i < n; i++) {
    h.insert(read());
  }
  for (int i = 0; i < m; i++) {
    int c = read();
    if (h.size() == 0) {
      std::cout << "-1\n";
      continue;
    }
    auto it = h.upper_bound(c);
    if (it == h.begin()) {
      std::cout << "-1\n";
    } else {
      it = std::prev(it);
      std::cout << *it << '\n';
      h.erase(it);
    }
  }
  return 0;
}

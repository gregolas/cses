#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

template <typename T = int>
inline T read(){
  T t = 0;
  char c;
  while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
    t=t*10+c-48;
  }
  return t;
}

int main(void) {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n = read();
  int ans = 0;
  std::vector<bool> seen;
  seen.resize(n + 1);
  for (int i = 0; i < n; i++) {
    int curr = read();
    ans += not seen[curr - 1];
    seen[curr] = true;
  }
  std::cout << ans << '\n';
  return 0;
}

#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

constexpr int mod = 1e9 + 7;

template <typename T = int>
inline T read(){
  T t = 0;
  char c;
  while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
    t=t*10+c-48;
  }
  return t;
}

int main(void) {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n = read();
  int m = read();
  std::vector<int> v(n);
  std::vector<bool> seen(n + 1);
  std::vector<int> idx(n + 1);
  int ans = 0;
  idx[0] = 1e9;
  for (int i = 0; i < n; i++) {
    v[i] = read();
    idx[v[i]] = i;
    ans += not seen[v[i] - 1];
    seen[v[i]] = true;
  }
  for (int i = 0; i < m; i++) {
    int a = read() - 1;
    int b = read() - 1;
    int va = v[a];
    int vb = v[b];

    std::set<int> s;
    s.insert(va);
    s.insert(vb);
    if (va - 1 > 0) s.insert(va - 1);
    if (vb - 1 > 0) s.insert(vb - 1);
    if (va + 1 <= n) s.insert(va + 1);
    if (vb + 1 <= n) s.insert(vb + 1);

    //std::cout << "-------------" << std::endl;

    for (auto f : s) {
      int x = idx[f] < idx[f - 1];
      //std::cout << "adjusting down " << f << " " << x << std::endl;
      ans -= x;
    }

    //std::cout << v[a] << ", " << b << "  " << v[b] << ", " << a << std::endl;
    idx[v[a]] = b;
    idx[v[b]] = a;
    v[b] = va;
    v[a] = vb;

    for (int j = 1; j <= n; j++) {
      //std::cout << j << ": " << idx[j] << ", ";
    }
    //std::cout << std::endl;

    for (auto f : s) {
      int x = idx[f] < idx[f - 1];
      //std::cout << "adjusting up " << f << " " << x << std::endl;
      ans += x;
    }

    std::cout << ans << '\n';
  }
  return 0;
}

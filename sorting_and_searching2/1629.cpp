#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

template <typename T = int>
inline T read(){
  T t = 0;
  char c;
  while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
    t=t*10+c-48;
  }
  return t;
}

int main(void) {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n = read();
  std::vector<std::pair<int, int>> v;
  v.reserve(n);
  for (int i = 0; i < n; i++) {
    int a = read();
    int b = read();
    v.emplace_back(a, b);
  }
  std::sort(v.begin(), v.end());
  int ans = 0;
  int cur_end = 0;
  for (const auto &p : v) {
    if (p.first >= cur_end) {
      ans++;
      cur_end = p.second;
    } else if (p.second < cur_end) {
      cur_end = p.second;
    }
  }
  std::cout << ans << '\n';
  return 0;
}

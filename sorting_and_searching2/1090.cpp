#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

template <typename T = int>
inline T read(){
	T t = 0;
	char c;
	while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
		t=t*10+c-48;
	}
	return t;
}

int v[200001];

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n = read();
    int x = read();
    for (int i = 0; i < n; i++) v[i] = read();
    std::sort(v, v + n);
    int p1 = 0;
    int p2 = n - 1;
    int ans = 0;
    while (p1 < p2) {
      ans++;
      p1 += v[p1] + v[p2] <= x;
      p2--;
    }
    ans += p1 == p2;
    std::cout << ans << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

template <typename T = int>
inline T read(){
  T t = 0;
  char c;
  while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
    t=t*10+c-48;
  }
  return t;
}

int main(void) {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n = read();
  std::vector<ll> v; v.reserve(n);
  for (int i = 0; i < n; i++) {
    v.emplace_back(read<ll>());
  }
  std::sort(v.begin(), v.end());
  ll curr = 0;
  for (size_t i = 0; i < v.size(); i++) {
    if (curr + 1 < v[i]) break;
    curr += v[i];
  }
  std::cout << curr + 1 << '\n';
  return 0;
}

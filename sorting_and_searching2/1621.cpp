#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    std::set<int> s;
    for (int i = 0; i < n; i++) {
      int f;
      cin >> f;
      s.insert(f);
    }
    std::cout << s.size() << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int N, M, K;
    cin >> N >> M >> K;
    std::vector<int> desired;
    std::vector<int> apts;
    desired.resize(N);
    apts.resize(M);
    for (int i = 0; i < N; i++) {
      cin >> desired[i];
    }
    for (int i = 0; i < M; i++) {
      cin >> apts[i];
    }
    std::sort(desired.begin(), desired.end());
    std::sort(apts.begin(), apts.end());
    size_t apt_ptr = 0;
    size_t desired_ptr = 0;
    int ans = 0;
    while (apt_ptr < apts.size() and desired_ptr < desired.size()) {
      if (desired[desired_ptr] > apts[apt_ptr] + K) {
        apt_ptr++;
        continue;
      }
      if (desired[desired_ptr] < apts[apt_ptr] - K) {
        desired_ptr++;
        continue;
      }
      desired_ptr++;
      apt_ptr++;
      ans += 1;
    }
    std::cout << ans << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

constexpr int mod = 1e9 + 7;

template <typename T = int>
inline T read(){
  T t = 0;
  char c;
  while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
    t=t*10+c-48;
  }
  return t;
}

int main(void) {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  std::set<std::pair<int, int>> segments;
  std::multiset<int> lengths;
  int x = read();
  int n = read();
  segments.emplace(0, x);
  lengths.emplace(x);
  for (int i = 0; i < n; i++) {
    int t = read();
    auto it = segments.upper_bound(std::make_pair(t, 1e9));
    if (it == segments.begin()) {
      std::cout << "wtf" << std::endl;
    } else {
      it = std::prev(it);
      int length = it->second - it->first;
      std::pair<int, int> new_segment_a{it->first, t};
      std::pair<int, int> new_segment_b{t, it->second};
      lengths.erase(lengths.find(length));
      lengths.emplace(t - it->first);
      lengths.emplace(it->second - t);
      auto iit = segments.insert(it, new_segment_b);
      segments.insert(iit, new_segment_a);
      segments.erase(it);
    }
    std::cout << *std::prev(lengths.end()) << '\n';
  }
  return 0;
}

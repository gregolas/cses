#include <iostream>
#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>
#include <limits>

using namespace std;

using ll = long long;
using ull = unsigned long long;

template <typename T = int>
inline T read(){
  T t = 0;
  char c;
  while((c=getchar_unlocked())!='\n'&&c!=' '&&c!=EOF){
    t=t*10+c-48;
  }
  return t;
}

int main(void) {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n = read();
  int x = read();
  std::vector<std::pair<int, int>> v; v.reserve(n);
  for (int i = 0; i < n; i++) {
    v.emplace_back(read(), i + 1);
  }
  std::sort(v.begin(), v.end());
  int p1 = 0, p2 = n - 1;
  while (p1 < p2) {
    int sum = v[p1].first + v[p2].first;
    if (sum == x) {
      int a1 = v[p1].second;
      int a2 = v[p2].second;
      std::cout << std::min(a1, a2) << ' ' << std::max(a1, a2) << '\n';
      return 0;
    } else if (sum < x) {
      p1++;
    } else {
      p2--;
    }
  }
  std::cout << "IMPOSSIBLE\n";
  return 0;
}

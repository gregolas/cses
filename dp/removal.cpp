#include <iostream>
#include <vector>
#include <numeric>

typedef long long ll;

using namespace std;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    std::cin >> n;
    vector<ll> arr(n);
    ll sum = 0;
    for (int i = 0; i < n; i++) {
        std::cin >> arr[i];
        sum += arr[i];
    }
    vector<vector<ll>> dp(n, vector<ll>(n));
    for (int i = 1; i <= n; i++) {
        bool p1 = i % 2 == n % 2;
        int mult = p1 ? 1 : -1;
        auto func = p1 ? std::max<ll> : std::min<ll>;
        for (int j = 0; j + i - 1 < n; j++) {
            if (i == 1) {
                dp[j][j] = mult * arr[j];
                continue;
            }
            dp[j][j + i - 1] = func(mult * arr[j] + dp[j + 1][j + i - 1], mult * arr[j + i - 1] + dp[j][j + i - 2]);
        }
    }
    ll diff = dp[0].back();
    ll p2 = sum / 2;
    ll p1 = sum - p2;
    diff -= (p1 - p2);
    p1 += diff / 2;
    std::cout << p1 << std::endl;
    return 0;
}

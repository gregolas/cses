#include <iostream>
#include <vector>

using namespace std;

int main(void) {
    constexpr int MOD = 1e9 + 7;
    int n;
    std::cin >> n;
    vector<vector<char>> grid(n, vector<char>(n));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            std::cin >> grid[i][j];
        }
    }
    vector<vector<int>> dp(n, vector<int>(n));
    if (grid[0][0] != '*') {
        dp[0][0] = 1;
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (grid[i][j] == '*') continue;
            dp[i][j] += i ? dp[i - 1][j] : 0;
            dp[i][j] %= MOD;
            dp[i][j] += j ? dp[i][j - 1] : 0;
            dp[i][j] %= MOD;
        }
    }
    std::cout << dp.back().back() << std::endl;
    return 0;
}

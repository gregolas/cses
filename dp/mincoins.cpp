#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(void) {
    int n, x;
    std::cin >> n >> x;
    vector<int> coins(n);
    for (int i = 0; i < n; i++) {
        std::cin >> coins[i];
    }
    vector<int> dp(x + 1, std::numeric_limits<int>::max());
    dp[0] = 0;
    std::sort(coins.begin(), coins.end(), std::greater<int>());
    for (int i = 0; i < coins.size(); i++) {
        int coin = coins[i];
        for (int j = 0; j < dp.size(); j++) {
            if (dp[j] != std::numeric_limits<int>::max() and j + coin < dp.size()) {
                dp[j + coin] = std::min(dp[j + coin], dp[j] + 1);
            }
        }
    }
    std::cout << (dp.back() == std::numeric_limits<int>::max() ? -1 : dp.back()) << std::endl;
    return 0;
}

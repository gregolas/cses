#include <iostream>
#include <vector>

using namespace std;

int main(void) {
    int n, x;
    std::cin >> n >> x;
    vector<int> prices(n);
    vector<int> pages(n);
    for (int i = 0; i < n; i++) {
        std::cin >> prices[i];
    }
    for (int i = 0; i < n; i++) {
        std::cin >> pages[i];
    }
    vector<vector<int>> dp(n, vector<int>(x + 1));
    int ans = 0;
    dp[0][prices[0]] = pages[0];
    for (int i = 1; i < dp.size(); i++) {
        for (int j = 1; j < dp[i].size(); j++) {
            dp[i][j] = dp[i - 1][j];
            if (j - prices[i] >= 0)
                dp[i][j] = std::max(dp[i - 1][j], dp[i - 1][j - prices[i]] + pages[i]);
            ans = std::max(ans, dp[i][j]);
        }
    }
    std::cout << ans << std::endl;
    return 0;
}

#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <limits>

using namespace std;

typedef unsigned long long ull;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    std::cin >> n;
    vector<vector<ull>> v(n, vector<ull>(3));
    for (int i = 0; i < n; i++) {
        std::cin >> v[i][0];
        std::cin >> v[i][1];
        std::cin >> v[i][2];
    }

    std::sort(v.begin(), v.end(), [](const vector<ull> &v1, const vector<ull> &v2) {
        return v1[1] < v2[1];
    });

    // max points using up to the i-th job
    vector<ull> dp(n);
    // end time of last job used at dp[i]
    vector<ull> foo(n, std::numeric_limits<ull>::max());
    dp[0] = v[0][2];
    foo[0] = v[0][1];

    for (int i = 1; i < n; i++) {
        auto it = std::lower_bound(foo.begin(), foo.end(), v[i][0]);
        int idx = it - foo.begin() - 1;

        // base case - don't use
        dp[i] = dp[i - 1];
        foo[i] = foo[i - 1];

        // use - either by itself or with sequence ending at dp[idx]
        ull new_dp = idx < 0 ? v[i][2] : v[i][2] + dp[idx];
        if (new_dp > dp[i]) {
            dp[i] = new_dp;
            foo[i] = v[i][1];
        } else if (new_dp == dp[i]) {
            foo[i] = std::min(v[i][1], foo[i]);
        }
    }

    std::cout << dp.back() << '\n';

    return 0;
}

#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main(void) {
    constexpr int MOD = 1e9 + 7;
    int n, x;
    std::cin >> n >> x;
    vector<int> coins(n);
    for (int i = 0; i < n; i++) {
        std::cin >> coins[i];
    }
    vector<int> dp(x + 1);

    dp[0] = 1;

    std::sort(coins.begin(), coins.end(), std::greater<int>());

    for (int j = 0; j < coins.size(); j++) {
        for (int i = 0; i < dp.size(); i++) {
        int coin = coins[j];
            if (dp[i] and i + coin < dp.size()) {
                dp[i + coin] += dp[i];
                dp[i + coin] %= MOD;
            }
        }
    }
    std::cout << dp.back() << std::endl;

    return 0;
}

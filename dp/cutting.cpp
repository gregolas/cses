#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <tuple>

using namespace std;

int go(int x1, int x2, int y1, int y2) {
    static vector<vector<int>> memo(501, vector<int>(501, -1));
    if (x2 - x1 > y2 - y1) {
        std::swap(x1, y1);
        std::swap(x2, y2);
    }
    if (memo[x2 - x1][y2 - y1] != -1) {
        return memo[x2 - x1][y2 - y1];
    }
    int res = 1e9;
    if (x2 - x1 == y2 - y1) {
        res = 0;
    }
    else if (x1 == x2) {
        res = y2 - y1;
    } else if (y1 == y2) {
        res = x2 - x1;
    }
    else {
        for (int i = 0; i < x2 - x1; i++) {
            int a1 = go(x1, x1 + i, y1, y2);
            int a2 = go(x1 + i + 1, x2, y1, y2);
            res = std::min(res, a1 + a2 + 1);
        }
        for (int j = 0; j < y2 - y1; j++) {
            int a1 = go(x1, x2, y1, y1 + j);
            int a2 = go(x1, x2, y1 + j + 1, y2);
            res = std::min(res, a1 + a2 + 1);
        }
    }
    memo[x2 - x1][y2 - y1] = res;
    return res;
}

int main(void) {
    int a, b;
    std::cin >> a >> b;
    std::cout << go(0, a - 1, 0, b - 1) << std::endl;
    return 0;
}

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

int main(void) {
    std::string s1, s2;
    std::cin >> s1;
    std::cin >> s2;

    int MAX = std::numeric_limits<int>::max();
    vector<vector<int>> dp(s1.size() + 1, vector<int>(s2.size() + 1, MAX));

    dp[0][0] = 0;
    for (int i = 0; i < dp[0].size(); i++) {
        dp[0][i] = i;
    }
    for (int i = 0; i < dp.size(); i++) {
        dp[i][0] = i;
    }

    for (size_t i = 1; i < dp.size(); i++) {
        for (size_t j = 1; j < dp[i].size(); j++) {
            if (dp[i - 1][j - 1] != MAX) {
                dp[i][j] = std::min(dp[i][j], dp[i - 1][j - 1] + static_cast<int>(s1[i - 1] != s2[j - 1]));
                dp[i][j] = std::min(dp[i][j], dp[i - 1][j - 1] + 1);
            }
            if (dp[i][j - 1] != MAX) {
                dp[i][j] = std::min(dp[i][j], dp[i][j - 1] + 1);
            }
            if (dp[i - 1][j] != MAX) {
                dp[i][j] = std::min(dp[i][j], dp[i - 1][j] + 1);
            }
        }
    }
    std::cout << dp.back().back() << std::endl;
    return 0;
}

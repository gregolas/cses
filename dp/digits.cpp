#include <iostream>
#include <vector>

using namespace std;

int main(void) {
    int n;
    std::cin >> n;

    vector<int> dp(n + 1, 1e9);
    dp[n] = 0;
    for (int i = n; i > 0; i--) {
        int c = i;
        while (c) {
            int r = c % 10;
            c /= 10;
            if (i - r >= 0) {
                dp[i - r] = std::min(dp[i - r], dp[i] + 1);
            }
        }
    }
    std::cout << dp[0] << std::endl;
    return 0;
}

#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <sstream>

using namespace std;

int main(void) {
    int n;
    std::cin >> n;
    vector<int> nums(n);
    int max = 0;
    for (int i = 0; i < n; i++) {
        std::cin >> nums[i];
        max = std::max(max, nums[i]);
    }
    vector<bool> dp(n * max + 1, false);
    dp[0] = true;
    for (int i = 0; i < nums.size(); i++) {
        vector<bool> dp_next(n * max + 1, false);
        int num = nums[i];
        for (int j = 0; j < dp.size(); j++) {
            dp_next[j] = dp[j];
            if (j - num >= 0 and dp[j - num]) {
                dp_next[j] = true;
            }
        }
        dp = std::move(dp_next);
    }
    std::stringstream ss;
    int ans = 0;
    for (int i = 1; i < dp.size(); i++) {
        if (dp[i]) {
            ss << i << " ";
            ans++;
        }
    }
    std::cout << ans << '\n';
    std::cout << ss.str() << '\n';
    return 0;
}

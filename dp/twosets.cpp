#include <iostream>
#include <vector>

using namespace std;

typedef unsigned int ull;

int main(void) {
    constexpr int MOD = 1e9 + 7;
    int n;
    std::cin >> n;

    int foo = n * (n + 1);
    if (foo % 4 != 0) {
        std::cout << "0\n";
        return 0;
    }
    vector<ull> dp(foo / 4 + 1);
    dp[0] = 1;
    ull ans = 0;
    for (int i = n; i > 0; i--) {
        vector<ull> dp_new(foo / 4 + 1);
        for (int j = 0; j < dp.size(); j++) {
            dp_new[j] = dp[j];
            if (j - i >= 0 and dp[j - i]) {
                dp_new[j] += dp[j - i];
                dp_new[j] %= MOD;
            }
            if (i == 1 and j == dp.size() - 1) {
                ans += dp[j - 1];
                ans %= MOD;
            }
        }
        dp = std::move(dp_new);
    }
    std::cout << ans << '\n';
    return 0;
}

#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

template <typename T>
void add(T &a, T b) {
    constexpr int MOD = 1e9 + 7;
    a += b;
    a %= MOD;
}

int main(void) {
    int n, m;
    std::cin >> n >> m;
    vector<int> arr(n);
    for (int i = 0; i < n; i++) {
        std::cin >> arr[i];
    }
    vector<vector<int>> dp(n, vector<int>(m + 1));
    if (arr[0] != 0) {
        dp[0][arr[0]] = 1;
    } else {
        std::fill(dp[0].begin() + 1, dp[0].end(), 1);
    }
    for (int i = 1; i < n; i++) {
        if (arr[i] == 0) {
            for (int j = 1; j <= m; j++) {
                if (j > 1) add(dp[i][j], dp[i - 1][j - 1]);
                if (j < m) add(dp[i][j], dp[i - 1][j + 1]);
                add(dp[i][j], dp[i - 1][j]);
            }
        } else {
            int v = arr[i];
            add(dp[i][v], dp[i - 1][v]);
            if (v > 1) add(dp[i][v], dp[i - 1][v - 1]);
            if (v < m) add(dp[i][v], dp[i - 1][v + 1]);
        }
    }
    int ans = 0;
    for (int i = 1; i <= m; i++) {
        add(ans, dp.back()[i]);
    }
    std::cout << ans << std::endl;
    return 0;
}

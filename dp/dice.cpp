#include <iostream>
#include <vector>

using namespace std;

int main(void) {
    constexpr int MOD = 1e9 + 7;
    int n;
    std::cin >> n;
    vector<int> dp(n + 1);
    dp[0] = 1;
    for (int j = 0; j < dp.size(); j++) {
        for (int i = 1; i <= 6; i++) {
            if (dp[j] and j + i < dp.size()) {
                dp[j + i] += dp[j];
                dp[j + i] %= MOD;
            }
        }
    }
    std::cout << dp.back() << std::endl;
    return 0;
}

#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <utility>
#include <unordered_map>
#include <cassert>

using namespace std;

typedef long long ll;

// inclusive
ll max(const vector<ll> &arr, int start, int end, bool p1turn, ll sum) {
    static unordered_map<int, unordered_map<int, ll>> memo;
    if (start == end) {
        return arr[start];
    }
    if (auto it = memo.find(start); it != memo.end()) {
        if (auto it2 = it->second.find(end); it2 != it->second.end()) {
            return it2->second;
        }
    }
    /*int sum_a = std::accumulate(arr.begin() + start + 1, arr.begin() + end + 1, 0);
    int sum_b = std::accumulate(arr.begin() + start, arr.begin() + end, 0);*/
    ll sum_a = sum - arr[start];
    ll sum_b = sum - arr[end];
    ll a = arr[start] + (sum_a - max(arr, start + 1, end, !p1turn, sum_a));
    ll b = arr[end] + (sum_b - max(arr, start, end - 1, !p1turn, sum_b));
    memo[start][end] = std::max(a, b);
    return memo[start][end];
}

/*pair<int, int> max2(const vector<int> &arr, int start, int end, bool p1turn) {
    if (start == end) {
        if (p1turn) {
            return {arr[start], 0};
        } else {
            return {0, arr[start]};
        }
    }
    int a = 
}*/

int main(void) {
    int n;
    std::cin >> n;
    vector<ll> arr(n);
    for (int i = 0; i < n; i++) {
        std::cin >> arr[i];
    }
    ll sum = std::accumulate(arr.begin(), arr.end(), 0);
    std::cout << max(arr, 0, arr.size() - 1, true, sum) << std::endl;
    return 0;
}

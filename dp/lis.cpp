#include <iostream>
#include <vector>
#include <algorithm>
#include <bits/stdc++.h>

using namespace std;

int main(void) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;
    vector<int> dp{};
    dp.reserve(n / 2);
    for (int i = 0; i < n; i++) {
        int curr;
        cin >> curr;
        auto it = lower_bound(dp.begin(), dp.end(), curr);
        if (it == dp.end()) dp.push_back(curr);
        else dp[it - dp.begin()] = curr;
    }
    cout << dp.size() << '\n';
    return 0;
}
